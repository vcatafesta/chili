#!/usr/bin/env bash

reset=$(tput sgr0);
green=$(tput setaf 2);
red=$(tput setaf 124);
pink=$(tput setaf 129);

#debug
export PS4=$'${red}${0##*/}${green}[$FUNCNAME]${pink}[$LINENO]${reset} '
#set -x

#http_user='nginx'
http_user='http'

# Lockfile path
#lock="/var/lock/voidrepo.lck"

#source_url="rsync://repo-us.voidlinux.org/voidlinux"
#source_url="rsync://repo-fi.voidlinux.org/voidlinux"
#source_url="rsync://repo-de.voidlinux.org/voidlinux"
source_url="rsync://mirrors.servercentral.com/voidlinux"
#source_url="rsync://100.97.0.15/voidlinux"

# Directory where the repo is stored locally. Example: /srv/repo
target="/vg/mirror-data/voidlinux"

# If you want to limit the bandwidth used by rsync set this.
# Use 0 to disable the limit.
# The default unit is KiB (see man rsync /--bwlimit for more)
bwlimit=0

# An HTTP(S) URL pointing to the 'lastupdate' file on your chosen mirror.
# If you are a tier 1 mirror use: https://rsync.archlinux.org/lastupdate
# Otherwise use the HTTP(S) URL from your chosen mirror.
lastupdate_url=''

#### END CONFIG

main_alan() {
	sudo -u $http_user rsync -avHP --delete --exclude="debug" $source_url $target
}

main_last_work() {
	rsync								\
		--chown=root:$http_user	\
		--human-readable			\
		--archive					\
		--delete						\
	  	--delete-delay				\
	  	--delay-updates 			\
		--recursive					\
		--links						\
		--perms						\
		--times						\
		--compress					\
		--partial					\
		--hard-links				\
		--exclude 'debug' 		\
		--exclude 'aarch64' 		\
		--exclude '*armv7l*' 	\
		--exclude '*armv6l*' 	\
		--exclude '*musl*' 		\
		--exclude '*i686*' 		\
		--exclude '*noarch*' 	\
		$source_url					\
		$target
}
#		--dry-run \
#		--copy-as=root:$http_user	\
#		--verbose						\
#		--progress						\

main_partial() {
	local i
	local -a adir=(
			currrent
			distfiles
			docs
			live
			logos
			man
			static
			void-updates
			xlocate
		)

	for i in "${adir[@]}"
	do
		printf "%s\n" "Syncing from server : $source_url/$i"
		rsync							\
			--archive				\
			--human-readable		\
			--verbose				\
			--partial				\
			--progress				\
			--hard-links			\
			--exclude 'debug'		\
		  	--compress				\
		  	--progress				\
			$source_url/"$i"		\
			$target
	done
}
#			--exclude 'aarch64'	\
#			--exclude '*armv7l*'	\
#			--exclude '*armv6l*'	\

rsync_extras() {
	source_url='/vg/mirror-data/extras/'
	target='/vg/mirror-data/voidlinux/extras/'
	target1='/vg/mirror-data/voidlinux/current/extras/'

	echo
	prepare
#	rsync -avHP --delete $source_url $target
#	rsync -avHP --delete $source_url $target1
	rsync -aH --delete $source_url $target
	rsync -aH --delete $source_url $target1
	unset source_url
	unset target
	unset target1
}

rsync_rootfs() {
	source_url='/vg/mirror-data/rootfs/'
	target='/vg/mirror-data/voidlinux/live/current/'

	echo
	prepare
#	rsync -avHP $source_url $target
	rsync -aH $source_url $target
	cd $target || return
	ln -sf void-x86_64-base-minimal-20230131.tar.xz void-x86_64-base-minimal-current.tar.xz
	ln -sf void-x86_64-base-system-20230112.tar.xz void-x86_64-base-system-current.tar.xz
	ln -sf void-x86_64-base-voidstrap-20230103.tar.xz void-x86_64-base-voidstrap-current.tar.xz
	unset source_url
	unset target
}

rsync_cmd() {
	local -a cmd=(rsync
		--archive
		--delete
		--recursive
		--links
		--perms
		--times
		--compress
		--progress
		--partial
		--hard-links
     	"--timeout=600"
	  	"--contimeout=60"
	  	--no-motd
	  	--delete-delay
	  	--delay-updates
 		--safe-links
		)

	if stty &>/dev/null; then
		cmd+=(--human-readable --verbose --progress)
	else
		cmd+=(--quiet)
	fi

	if ((bwlimit>0)); then
		cmd+=("--bwlimit=$bwlimit")
	fi

	"${cmd[@]}" "$@"
}

prepare() {
	printf "%s\n" "${reset}Creating target dir : ${pink}$target${reset}"
	[[ ! -d "${target}" ]] && mkdir -p "${target}"

	#exec 9>"${lock}"
	#flock -n 9 || exit

	# Cleanup any temporary files from old run that might remain.
	# find "${target}" -name '.~tmp~' -exec rm -r {} +

	printf "%s\n" "${reset}Syncing from server : ${pink}$source_url${reset}"
	printf "%s\n" "${reset}Destdir             : ${pink}$target${reset}"
}

main() {
	# if we are called without a tty (cronjob) only run when there are changes
	if ! tty -s && [[ -f "$target/lastupdate" ]] && diff -b <(curl -Ls "$lastupdate_url") "$target/lastupdate" >/dev/null; then
		# keep lastsync file in sync for statistics generated
		rsync_cmd "$source_url/lastsync" "$target/lastsync"
		exit 0
	fi

	rsync_cmd 				\
		--exclude 'debug'	\
		"${source_url}"	\
		"${target}"

	echo "Last sync was " "$(date -d @"$(cat ${target}/lastsync)")"
}

prepare
main_last_work
#main_partial
#main
rsync_extras
rsync_rootfs

# vim:set ts=3 sw=3 et:
# shellcheck OK
