#!/usr/bin/env bash

conf() {
	read -p "$1 [Y/n]"
	[[ ${REPLY^} == "" ]] && return 0
	[[ ${REPLY^} == N ]]  && return 1 || return 0
}

sh_setvarcolors() {
	# does the terminal support true-color?
	if [[ $(tput colors) -eq 256 ]]; then
		#tput setaf 127 | cat -v  #capturar saida
		tput sgr0 # reset colors
		bold=$(tput bold)
		reset=$(tput sgr0)
		black=$(tput setaf 0);
		red=$(tput setaf 1)
		green=$(tput setaf 2)
		yellow=$(tput bold)$(tput setaf 3)
		blue=$(tput setaf 4)
		pink=$(tput setaf 5)
		cyan=$(tput setaf 6)
		white=$(tput setaf 7)
		orange=$(tput setaf 3)
		purple=$(tput setaf 125);
		violet=$(tput setaf 61);
   else # doesn't support true-color
		bold=''
		reset="\e[0m"
		blue="\e[1;34m"
		cyan="\e[1;36m"
		green="\e[1;32m"
		orange="\e[1;33m"
		red="\e[1;31m"
		white="\e[1;37m"
		yellow="\e[1;33m"
		pink="\033[35;1m"
		black="\e[1;30m";
		purple="\e[1;35m";
		violet="\e[1;35m";
   fi
}

chili-image() {
	# https://www.qemu.org/2021/08/22/fuse-blkexport/
	qemu-img create -f raw foo.img 20G
	parted -s foo.img 		\
		'mklabel msdos' 		\
		'mkpart primary ext4 2048s 100%'
	qemu-img convert -p -f raw -O qcow2 foo.img foo.qcow2 && rm foo.img
	file foo.qcow2
	kpartx -l foo.qcow2

#	qemu-storage-daemon \
#    --blockdev node-name=prot-node,driver=file,filename=foo.qcow2 \
#    --blockdev node-name=fmt-node,driver=qcow2,file=prot-node \
#    --export \
#    type=fuse,id=exp0,node-name=fmt-node,mountpoint=foo.qcow2,writable=on \
#    &

	qemu-storage-daemon \
		--blockdev node-name=prot-node,driver=file,filename=foo.qcow2 \
		--blockdev node-name=fmt-node,driver=qcow2,file=prot-node \
		--export \
		type=fuse,id=exp0,node-name=fmt-node,mountpoint=mount-point,writable=on

	file foo.qcow2
	sudo kpartx -av foo.qcow2
	fdisk -l foo.qcow2
	cfdisk foo.qcow2
}

chili-mapdevice()
{
	#sudo mknod -m 0777 /dev/sr0 b 7 10
	#sudo mknod -m 0777 /dev/sr0 b 7 20
	# losetup -P /dev/sr0 $1
	kpartx -av disk.img # mapear
	kpartx -d disk.img  #delete

	# dd if=/dev/zero of=~/file.img bs=1024k count=10
	# losetup --find --show ~/file.img # /dev/loop0
	# mkfs -t ext2 /dev/loop0
	# mount /dev/loop0 /mnt
	# umount /dev/loop0
	# losetup --detach /dev/loop0
}

chili-setup-img()
{
	qemu-img create foo.img -f raw 20G
   qemu-img info foo.img
	losetup --find --show foo.img
	#kpartx -av foo.img # mapear
	#kpartx -d foo.img  #delete
}

#sd=$1
#parted --script $sd -- 															\
#   mklabel gpt             													\
#   mkpart primary fat32      1MiB   100MiB set 1 bios on name 1 BIOS  \
#   mkpart primary fat32      100MiB 200MiB set 2 esp  on name 2 EFI   \
#   mkpart primary linux-swap 200MiB 2GB                  name 3 SWAP  \
#   mkpart primary ext4       2GB 100%                    name 4 ROOT  \
#   align-check optimal 1
#parted --script $sd -- print

#Partição 	Sistema de arquivo 	Tamanho 	Descrição
#/dev/sda1 	(bootloader) 	2M 	Partição de boot BIOS
#/dev/sda1 	ext2 (ou FAT32 se UEFI está sendo usada) 	128M 	Partição boot/Sistema EFI
#/dev/sda3 	(swap) 	512M ou maior 	Partição de swap
#/dev/sda4 	ext4 	Resto do disco 	Partição root

sh_parted() {
	sd="$1"
	if fdisk -l $sd; then
		echo
		echo "AVISO, CUIDADO, tudo será apagado!!"
		if conf "Continuar com o particionamento?" ; then
			parted --script "$sd" -- 															\
				mklabel gpt             					 									\
			   mkpart primary fat32      1MiB 3MiB   set 1 bios on name 1 BIOS  	\
			   mkpart primary fat32      3MiB 128MiB set 2 esp  on name 2 EFI   	\
			   mkpart primary ext4       128MiB 100%               name 3 ROOT  	\
			   align-check optimal 1
				parted --script "$sd" -- print
		fi
	fi
}

sh_checkroot() {
	if [ "$(id -u)" != "0" ]; then
		printf "${red} error: You cannot perform this operation unless you are root!\n"
		exit 1
	fi
}

sh_setvarcolors
if [[ $# -eq 0 ]]; then
	printf '%s\n' "${orange}chili-parted - utilitario para particionar disco${reset}"
	printf '%s\n' "${red}Usage:${reset}"
	printf '\t%s\n' "chili-parted ${yellow}/dev/sdX${reset}"
	exit 1;
fi

sh_checkroot "$@"
sh_parted "$@"
